'use strict'
/** Gets values from the currency.json asynchroniously
 */
function initialize_Select_Options_data(){
    let js_BaseCurrency = document.querySelectorAll('select')[0];
    let js_ToCurrency = document.querySelectorAll('select')[1];
    let js_Amount = document.querySelector('input');
    fetch("json/currency.json")
    .then((resp)=>{return resp.json()})
    .then((resp)=>{
        let data = resp;
        js_Amount.value = 0;
        Object.values(data).forEach((e)=>{
            let option = document.createElement("option");
            option.setAttribute("value",e.code);
            option.textContent = e.name;
            js_BaseCurrency.append(option);

            let option2 = document.createElement("option");
            option2.setAttribute("value",e.code);
            option2.textContent = e.name;
            js_ToCurrency.append(option2);
        })
    })
    .catch((err)=>{console.log("error: ",err)});
}