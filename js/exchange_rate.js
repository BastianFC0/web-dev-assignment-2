'use strict'
let urlObject = {
    mainURL: 'https://api.exchangerate.host/convert'
    };
let formBase;
let formTo;
let formAmount;
window.addEventListener('DOMContentLoaded', init);
/** Allows everything to load after the asynch stuff is done. It initializes object values and sets the event listeners to listen to when they change.
 */
function init(){
    initialize_Select_Options_data();
    setTimeout(()=>{
    const button = document.querySelector('button');
    button.addEventListener('click', getData);
    formBase = document.querySelectorAll('select')[0];
    formTo = document.querySelectorAll('select')[1];
    formAmount = document.querySelector('input');
    urlObject.fromQry = `?from=${formBase.value}`;
    urlObject.toQry = `&to=${formTo.value}`;
    urlObject.amountQry = `&amount=${formAmount.value}`;
    formBase.addEventListener('change',()=>{
        urlObject.fromQry = `?from=${formBase.value}`;
    });
    formTo.addEventListener('change',()=>{
        urlObject.toQry = `&to=${formTo.value}`;
    });
    formAmount.addEventListener('change',()=>{
        urlObject.amountQry = `&amount=${formAmount.value}`;
    });
    }, 100);
}

/** Builds and
 *  @returns The absolute URL using the values of urlObject
 */
function getAbsoluteRequestURL(){
    return urlObject.absoluteURL = urlObject.mainURL+urlObject.fromQry+urlObject.toQry+urlObject.amountQry;
}

/** Deals with the data from the absolute URL, notably calls for the displayResults() function if everything goes correctly
 */
function getData(){
    let url  = getAbsoluteRequestURL();
    fetch(url)
    .then((resp)=>{ 
        if(resp.status == 200){
        return resp.json()
            }else{
                console.log('Network error');
            }
    })
    .then((resp)=>{
        displayResults(resp);
    })
    .catch((error)=>{"error: ",error})
}
/** Builds the table with the appropriate info. (Also deletes any previous tables)
 * 
 * @param {Object} inputObject An object returned by the website with the calculations already done
 */
function displayResults(inputObject){
    if(Math.sign(formAmount.value) == -1){
        alert("Don't input negative numbers please");
    }
    else{
        for(let i = 1; i < document.querySelector("tbody").children.length;){
            document.querySelector("tbody").lastElementChild.remove();
        }
        const lastRow =  document.querySelector("tbody");
        const tableRow = document.createElement("tr");
        lastRow.append(tableRow);
        for(let i = 0; i < 6; i++){
            let tableCell = document.createElement("td");
            let tableIndex = document.querySelectorAll("tr")[1];
            tableIndex.appendChild(tableCell);
        }
        let tableInnerCell = document.querySelectorAll("td");
        tableInnerCell[0].textContent = formBase.value;
        tableInnerCell[1].textContent = formTo.value;
        tableInnerCell[2].textContent = inputObject.info.rate;
        tableInnerCell[3].textContent = formAmount.value;
        tableInnerCell[4].textContent = inputObject.result;
        tableInnerCell[5].textContent = new Date().toLocaleDateString();
    }
}